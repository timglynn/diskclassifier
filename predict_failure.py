#!/usr/bin/env python

import pickle
from sklearn.naive_bayes import GaussianNB
import argparse

def classify(classifier_path, drive_age):
    with open(classifier_path, "rb") as f:
        classifier = pickle.load(f)
    # predict using classifier, 0 for healthy, 1 for failure
    prediction = classifier.predict([[int(drive_age)]])
    return prediction

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Use a Bayesian classifier to see if a drive of a certain age will fail")
    parser.add_argument("-c", "--classifier", help ="Path to trained classifier")
    parser.add_argument("age", help="Drive age")
    args = parser.parse_args()
    print(classify(args.classifier, args.age)[0])
