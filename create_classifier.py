#!/usr/bin/env python
from sklearn.naive_bayes import GaussianNB
import sqlite3
import pickle
import argparse

def make_classifier(db_path, out_path):
    # create naive bayes classifier
    classifier = GaussianNB()

    conn = sqlite3.connect(db_path)
    c = conn.cursor()

    # Get smart_9_raw, which indicates number of hours online
    X = [[row[0]] if type(row[0]) == int else [0] for row in c.execute('SELECT smart_9_raw from drive_stats where smart_9_raw is not NULL;') ]

    # Get failures
    Y = [row[0] for row in c.execute('SELECT failure from drive_stats where failure is not NULL;')]


    # Train classifier with failures based on run time
    classifier = classifier.fit(X, Y)

    # Store the trained model for faster use
    with open(out_path, "wb") as f:
        pickle.dump(classifier,f )

    conn.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Create a Bayesian classifier from hard disk data")
    parser.add_argument("-d", "--database", help ="Path to sqlite database with disk stats")
    parser.add_argument("-o", "--output", help="Saved classifier output")
    args = parser.parse_args()
    make_classifier(args.database, args.output)
